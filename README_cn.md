# jetbrians

基于 Docker 的 IntelliJIDEA 系列激活服务端

### 示例

请确认已经安装过`docker`！

```
docker run -p 1128:1128 dingdayu/jetbrians
```

更多请访问: [AT_DOCKER](docs/AT_DOCKER_zh.md)

### 激活

请选择: 

- [ ] JetBrains Account
- [ ] Activation code
- [x] License server

激活服务地址:

```
http://host:1128
```

> 请替换 `host` 为你的ip或者域名。

### nginx

```
server {
        listen       80;
        server_name ide.example.com;
        error_log  logs/ide.error.log;
        access_log  logs/ide.access.log  main;

        location / {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_pass http://127.0.0.1:1128;
            proxy_redirect http://127.0.0.1:1128 /;
        }
}
```

文件：[nginx.conf](nginx.conf)

### 声明

1. 不要非法使用它。.
2. 仅供学习，请支持正版.
3. 如有侵权，请联系删除.

### 支持

- IntelliJ IDEA>=7.0
- ReSharper>=3.1
- ReSharper>=Cpp 1.0
- dotTrace>=5.5
- dotMemory>=4.0
- dotCover>=1.0
- RubyMine>=1.0
- PyCharm>=1.0
- WebStorm>=1.0
- PhpStorm>=1.0
- AppCode>=1.0
- CLion>=1.0

### 链接

1. [IntelliJ IDEA License Server v1.5](http://blog.lanyus.com/archives/314.html)

### 赞助

<p align="center">
    <img width="400" src="https://github.com/dingdayu/jetbrians/blob/master/images/merge.png"></img>
</p>
